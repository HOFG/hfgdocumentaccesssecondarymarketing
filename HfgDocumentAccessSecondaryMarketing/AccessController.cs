﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Automation;

namespace HfgDocumentAccessSecondaryMarketing
{
    class AccessController
    {
       public AccessController()
        {
           //
        }

        public void CheckAccess()
        {
            if (EncompassApplication.CurrentLoan.GetCurrentLock() != null)
            {
                if (EncompassApplication.CurrentLoan.GetCurrentLock().LockedBy.Equals(EncompassApplication.CurrentUser.ID) == true)
                {
                    try
                    {
                        SecondaryMarketing secondaryMarketing = new SecondaryMarketing();
                        secondaryMarketing.HandlePermissions();
                    }
                    catch (Exception)
                    {
                        //
                    }
                }
            }
        }
    }
}
