﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.ComponentModel;

namespace HfgDocumentAccessSecondaryMarketing
{
    [Plugin]
    public class DocumentAccess
    {
        public DocumentAccess()
        {
            EncompassApplication.LoanOpened += new EventHandler(LoanOpened);
            EncompassApplication.LoanClosing += new EventHandler(LoanClosing);
        }

        private void LoanOpened(object sender, EventArgs e)
        {
            if (EncompassApplication.Screens.InvokeRequired == true)
            {
                EncompassApplication.Screens.Invoke(new EventHandler(LoanOpened), new object[] { sender, e });
            }
            else
            {
                AccessController accessController = new AccessController();
                accessController.CheckAccess();
            }
        }

        private void LoanClosing(object sender, EventArgs e)
        {
            //
        }
    }
}
