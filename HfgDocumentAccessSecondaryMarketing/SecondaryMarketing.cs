﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.BusinessObjects.Users;

namespace HfgDocumentAccessSecondaryMarketing
{
    class SecondaryMarketing
    {
        private const string secondaryMarketingTitle = "Secondary Marketing";
        private List<TrackedDocument> secondaryDocuments { get; set; }

        public SecondaryMarketing()
        {
            this.secondaryDocuments = new List<TrackedDocument>();
        }

        public void HandlePermissions()
        {
            GetSecondaryMarketingDocuments();
            ChangePermissions();
        }

        private void GetSecondaryMarketingDocuments()
        {
            foreach (TrackedDocument trackedDocument in EncompassApplication.CurrentLoan.Log.TrackedDocuments)
            {
                if (trackedDocument.Title.Equals(secondaryMarketingTitle) == true)
                {
                    secondaryDocuments.Add(trackedDocument);
                }
            }
        }

        private void ChangePermissions()
        {
            Role underwriter = EncompassApplication.Session.Loans.Roles.GetRoleByAbbrev("UW");
            Role secondary = EncompassApplication.Session.Loans.Roles.GetRoleByAbbrev("SD");
            Role compliance = EncompassApplication.Session.Loans.Roles.GetRoleByAbbrev("CO");
            Role postClosing = EncompassApplication.Session.Loans.Roles.GetRoleByAbbrev("PC");

            foreach (TrackedDocument trackedDocument in secondaryDocuments)
            {
                foreach (Role role in trackedDocument.RoleAccessList)
                {
                    if (role != null)
                    {
                        if (role.Abbreviation.Equals("UW") == false && role.Abbreviation.Equals("SD") == false &&
                               role.Abbreviation.Equals("CO") == false && role.Abbreviation.Equals("PC") == false)
                        {
                            trackedDocument.RoleAccessList.Remove(role);
                        }
                    }
                }

                trackedDocument.RoleAccessList.Add(underwriter);
                trackedDocument.RoleAccessList.Add(secondary);
                trackedDocument.RoleAccessList.Add(compliance);
                trackedDocument.RoleAccessList.Add(postClosing);
            }
        }
    }
}
